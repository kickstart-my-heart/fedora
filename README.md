# Fedora Kickstarts

[![pipeline status](https://gitlab.com/kickstart-my-heart/fedora/badges/main/pipeline.svg)](https://gitlab.com/kickstart-my-heart/fedora/-/commits/main) 

Kickstart files for Fedora.

This repo is part of my [Kickstart My Heart](https://gitlab.com/kickstart-my-heart) project, where I maintain automated installations for a variety of operating systems.

## Links
* **Main Repo:** https://gitlab.com/kickstart-my-heart/fedora

### System requirements
* Download the latest [Fedora Everything ISO](https://www.mirrorservice.org/sites/dl.fedoraproject.org/pub/fedora/linux/releases/).
* Requires a minimum of `64GB` disk space for a `headless` install.
* Requires a minimum of `128GB` disk space for a `workstation` install.

### Configuring build options
To configure build options, copy the default `config.yml`:
```sh
cp -fv files/default/config.yml files/override/config.yml
```
Inside the new `files/override/config.yml` file, you can make changes to the default `kickstart_vars` variable, which defines what Kickstart files will be built when running this playbook. The variable in this file will take priority over the variables in the default `config.yml` file.

This playbook uses [vaulted variables](https://docs.ansible.com/ansible/latest/user_guide/vault.html) to store passwords, mostly as a demonstration of one way this can be done. The vault password for the default passwords is `ansible`. You will be asked for this when running the `make` commands below.

### Compiling Kickstart config files and ISOs locally
Make any necessary changes to the `ks.cfg.j2` or files in the `override/` directory and run the below commands.

Enter the build environment (you must have Podman installed)
```sh
make
```

Inside the build environment:
```sh
make kickstart
```

### Booting this install (provisioning a 'Golden Master' VM image with Packer)
Inside the environment:
```sh
make packerconf
```

Now, back on the host, enter into the relevant `packer/` directory for your virtualisation platform.

And then initialise Packer and build the image (Note that I use the full path to the Packer binary in the Makefile because of [this bug](https://github.com/hashicorp/packer/issues/11081)):
```sh
make build
```

To clear previously-built files from the current directory (this works in both the root directory and in the Packer subdirectories):
```sh
make clean
```

### Booting this install (installing directly to physical hardware, or provisioning a VM without using Packer)
Make local bootable Kickstart `OEMDRV` ISOs that will autoinstall Fedora for us:
```sh
make kickstart
```

Next steps:
* Attach Fedora Everything ISO to disk bay 1.
* Attach this Kickstart `OEMDRV` ISO to bay 2.
* Boot host.
* Installation should start automatically.

### Post-install steps
```sh
# Change the password for the root account
sudo passwd root

# Change the password for the user account
sudo passwd user

# Change the active LUKS encryption key used for the installed system
sudo cryptsetup luksChangeKey /dev/sda3
```

### Config Options
Config options which can be provided in `config.yml`:

* `container_engine`
  * String (`podman` or `docker` only)
  * Default: *No Default.*
  * Controls whether the `@container-management` (Podman) package group gets installed by `dnf` during the Kickstart process, or whether we install `moby-engine` and `docker-compose` instead. If no value is provided, neither will be installed.
* `disk_encryption`
  * Boolean
  * Default: `false`
  * Defines whether LUKS encryption will be used for the primary disk.
* `disk_path`
  * String
  * Default: *No default.*
  * This variable has no default and must be specified for every entry in the `config.yml` file. This controls the path that the installer will use to decide which disk to write to. It is recommended to use a `/dev/disk/by-id/xxxxx` style path where possible, to avoid situations where the installer overwrites the wrong disk. For fresh virtual machines or image deployments it is generally safe to use something like `/dev/sda`.
* `enable_bpftools`
  * Boolean
  * Default: `false`
  * Controls whether the `bcc-tools` and `bpftrace` packages get installed. Useful for performance tracing, but they require a full compiler toolchain to be installed on the target system as a dependency.
* `enable_cockpit`
  * Boolean
  * Default: `false`
  * Enables the Cockpit WebUI for graphical management of the system, and enables the associated `cockpit.socket` in systemd. **Does not** allow connections to Cockpit through the firewall. By default, Cockpit will only be accessible from the local system without additional configuration.
* `enable_devtools`
  * Boolean
  * Default: `false`
  * Include a bunch of packages in the packageset which I usually always find myself installing on Workstation installs. Does not do anything unless `workstation_deployment` is also set.
* `enable_kdump`
  * Boolean
  * Default: `false`
  * Controls whether kdump is enabled through installing `kexec-tools` and enabling `kdump.service`.
* `enable_snapd`
  * Boolean
  * Default: `false`
  * Controls whether the `snapd` package gets installed by `dnf` during the Kickstart process.
* `enable_vagrant`
  * Boolean
  * Default: `false`
  * Controls whether Vagrant and the provider for using Vagrant with libvirt get installed.
* `enable_zfs`
  * Boolean
  * Default: `false`
  * Defines whether the OpenZFS kABI-tracking `kmod` repository will be enabled on the installed server. Note that this only installs the OpenZFS repo and associated GPG keys. The ZFS packages do not get installed during installation.
* `grub_flags`
  * String (`systemd.unified_cgroup_hierarchy=1`, `quiet`, etc.)
  * Default: *No Default.*
  * A list of command line flags to add to the GRUB config that will be used when booting the system. Please be aware that setting this will override the default of `crashkernel=auto`, so if you want to preserve that flag then please also add it to your configured `grub_flags` variable.
* `hostname`
  * String (`rhel8`, `localhost`, `any-string`, etc.)
  * Default: `rhel8`
  * Controls what hostname the deployed system will use.
* `network_interface`
  * String (`link`, `eno1`, `ens192`, etc.)
  * Default: `link`
  * Controls which network interface the network configuration will be applied to. You may want to statically define this if you want to deploy to a system with multiple active interfaces. Otherwise the default is `link`, which will apply the configuration to the first active ethernet interface.
* `network_type`
  * String (`none`, `static`, `dhcp`)
  * Default: `none`
  * Controls what kind of network setup the deployed machine will use. By default no network is configured. This is to allow for maximum flexibility when deployed.
* `ntp_server`
  * String (`10.10.10.1`, `time.cloudflare.com`, etc.)
  * Default: *No Default.*
  * Feed a single NTP server to the `timesource` command in the Kickstart file. If none are provided, NTP will be configured to use publicly-available NTS timeservers.
* `ospp_hardening`
  * Boolean
  * Default: `false`
  * Controls whether OSPP hardening is applied by the OpenSCAP Anaconda add-on as part of the installation process. This allows us to have a hardened system right from the first install.
* `packer_disk_gb`
  * Integer
  * Default: `64`
  * Specifies the disk size in gigabytes that Packer should use when provisioning a machine. This defaults to `64` and should not be lower than this value but can be set higher.
* `poweroff`
  * Boolean
  * Default: `false`
  * Controls whether the Kickstart file instructs the machine to power off at the end of the Kickstart run. This is useful for Packer or image-based installs. The default is to reboot the VM and eject the Kickstart ISO.
* `rpmfusion_free`
  * Boolean
  * Default: `false`
  * Controls whether the RPMFusion Free repository is installed and enabled during the Kickstart run.
* `rpmfusion_nonfree`
  * Boolean
  * Default: `false`
  * Controls whether the RPMFusion Non-Free repository is installed and enabled during the Kickstart run.
* `timezone`
  * String (`Etc/UTC`, `Europe/London`, etc.)
  * Default: `Etc/UTC`
  * Configure the timezone if desired. Defaults to UTC.
* `vmware_vhw_ver`
  * String
  * Default: `18`
  * Configure the Virtual Hardware version of the VMware VMs that Packer will deploy. A value of `13` provides compatibility with VMware 6.5 and includes support for EFI and Secure Boot. Higher values may provide more recently added features.
* `workstation_deployment`
  * Boolean
  * Default: `false`
  * Controls whether this is a workstation deployment. The system will use a graphical boot target and will set a few other options accordingly that make sense for Workstation installs.

### Changelog
* `35.2` - Ensure the DNF Automatic timer gets enabled properly if the `ospp_hardening` variable is defined but something other than `true`.
* `35.1` - Fresh release with new version numbering system, based aronud Fedora 35.
